# I - ⚖️ License

Copyright (C) 2021  Weiyang(Will) Xu

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

# II - What's This?
It's a bash TDD, code first, demo project for learning c language. 

- [Chinese version here](readme_cn.md)
todo

# III - Tools Used
## i. Operating System
  - [Fedora](https://getfedora.org/) [GNU/Linux](https://www.gnu.org/gnu/linux-and-gnu.en.html)
  - [Ubuntu](https://ubuntu.com/) [GNU/Linux](https://www.gnu.org/gnu/linux-and-gnu.en.html) [WSL](https://docs.microsoft.com/en-us/windows/wsl/install-win10)
## ii. C language editor: [emacs](https://www.gnu.org/software/emacs/)
PS. Emacs is a little difficult to newbies. Any other text editor or IDE can be used to replace emacs.
## iii. Complier [gcc](https://gcc.gnu.org/)
## iv. Thanks [GNU](https://www.gnu.org/) program for all the above utils.
## v. Main Guides Website
- [100+ C Program examples with Output for practice](https://www.studytonight.com/c/programs/)

# IV - Project Structure
Category file for taking notes and brief introduction of the package.

- Cd to the directory and run the test/**.bash to test the codes.

```bash
cd /<path>/<to>/learnBasicC/test/startWithBasicPrograms/
./helloWorld.bash
```

|Category|Code|Test|
|---|---|---|
|🎉[0 - For Newbie](src/startWithBasicPrograms/newbie.md)| 1. Install operate system.|
| |2. Make directory and move in.|
| |3. Edit the hallo world file.|
| |4. Compile and run the program.|
|✨[1 - Start with Basic Programs](src/startWithBasicPrograms/startWithBasicPrograms.md)|1. [Hello,world!](src/startWithBasicPrograms/helloWorld.c) |[Test and notes](tests/startWithBasicPrograms/helloWorld.bash)|
| | 2. [Taking Input from User](src/startWithBasicPrograms/takingInputFromUser.c)| [Test and notes](tests/startWithBasicPrograms/takingInputFromUser.bash) |
| | 3. [ASCII value of Character](src/startWithBasicPrograms/asciiValueOfCharacter.c)| [Test and notes](tests/startWithBasicPrograms/asciiValueOfCharacter.bash) |
| | 4. [How to use gets() function](src/startWithBasicPrograms/helloGets.c)| [Test and notes](tests/startWithBasicPrograms/helloGets.bash) |
| | 5. [Basic if else condition program](src/startWithBasicPrograms/ifElse.c)| [Test and notes](tests/startWithBasicPrograms/ifElse.bash) |
| | 6. [Switch Case and break](src/startWithBasicPrograms/switchCase.c)| [Test and notes](tests/startWithBasicPrograms/switchCase.bash) |
| | 7. []()| [Test and notes]() |
