#!/bin/bash

# Copyright (C) 2021  Weiyang(Will) Xu
# 
# This file is part of learn-basic-c.
# Learn-basic-c is free software: you can redistribute it and/or modify
# it under the term of the GNU General Public License version 3 or any
# later version, as specified in the readme.md file.

source ../checkExcepted.bash

# import test utils
readonly TEST_UTILS_DIR='../utils/'
readonly CHAR2ASCII='character2ascii'

# Change this url and name
readonly SOURCE_DIR='../../src/startWithBasicPrograms/'
readonly PROJECT_NAME='asciiValueOfCharacter'
# Files with this name will be delete when test run successful.
readonly TEMPORARY_NAME="temporary.$PROJECT_NAME"

source_file=$SOURCE_DIR$PROJECT_NAME.c
out_file=$TEMPORARY_NAME.out

# sed -i 's/\r//' <file_name> to fix problem that file name has \r
# the wrong file name looks like <file_name>\r
gcc $source_file -o $out_file

echo "Enter a character: "
read c

# get the ascii of c character.
gcc $TEST_UTILS_DIR$CHAR2ASCII.c -o $CHAR2ASCII.out
asciiC=`echo $c | ./$CHAR2ASCII.out`
`rm $CHAR2ASCII.out`

# Provide the except result here.
except="Enter a character: 
ASCII value of $c = $asciiC"

actual=`echo $c | ./$out_file`

# check the except and actual in ../checkExcepted.bash
checkExcepted
