/*
 * Copyright (C) 2021  Weiyang(Will) Xu
 * 
 * This file is part of learn-basic-c.
 * Learn-basic-c is free software: you can redistribute it and/or modify
 * it under the term of the GNU General Public License version 3 or any
 * later version, as specified in the readme.md file.
 */

#include <stdio.h>

int
main ()
{
  /* char array of size 50 */
  char str[50];

  printf ("Enter your complete name:\n");
  
  /* 
   * gets() takes only a single line at a time.
   * i.e all the words before hitting \n(enter key).
   * WARNING: implicit declaration of function ‘gets’
   */
  gets (str);

  printf ("Welcome, %s\n", str);
}
