/*
 * Copyright (C) 2021  Weiyang(Will) Xu
 * 
 * This file is part of learn-basic-c.
 * Learn-basic-c is free software: you can redistribute it and/or modify
 * it under the term of the GNU General Public License version 3 or any
 * later version, as specified in the readme.md file.
 */

#include <stdio.h>

int
main ()
{
  int number;
  printf ("Please enter a number:\n");
  scanf ("%d", &number);

  if (number < 100)
    {
      printf ("This number is less than 100!\n");
    }
  else if (number == 100)
    {
      printf ("This number is 100!\n");
    }
  else
    {
      printf ("This number is greater than 100!\n");
    }

  return 0;
}
