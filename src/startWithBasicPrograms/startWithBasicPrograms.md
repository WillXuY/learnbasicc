# I - ⚖️ License

Copyright (C) 2021  Weiyang(Will) Xu

This file is part of learn-basic-c.
Learn-basic-c is free software: you can redistribute it and/or modify
it under the term of the GNU General Public License version 3 or any
later version, as specified in the readme.md file.

# 0 - First the code standards
Code style is very important for a programmer, especially the newbie.
Take a free code style choice.

I try to use GNU code standards here.
- ps. I learned java before, so sometimes my code style will be similar to java code style.
